#!/usr/bin/env bash

# Logitec C920 web camera

# cvlc v4l2:///dev/video0 --v4l2-chroma h264 --sout '#rtp{sdp=rtsp://:8554/}'

# cvlc v4l2:///dev/video0:chroma="H264":width=640:height=480:fps=30 --sout '#rtp{sdp=rtsp://:8554/}' :demux=h264

gst-launch-1.0 rpicamsrc bitrate=1000000 ! video/x-h264,width=320,height=240,framerate=30/1 ! h264parse ! flvmux ! rtmpsink location="rtmp://video.gcs.uno:1935/test11011/station_x live=1"

