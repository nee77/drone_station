/*

    Этот скрипт держит связь с web-сервером через socket.io
    Отправляет параметры станции
    Принимает команды на исполнение

    Имеет API для общения с дроном

 */


// Переменные в Redis
/*
    adc_status
    adc_error
    adc0
    adc1
    adc2
    adc3
    adc4
    adc5
    adc6
    adc7
 */


const config = require('./config/config');

const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const server = new http.Server(app);


const io = require('socket.io-client');
const socket = io(config.IO_HOST + '?me=station');


// Redis client init
const redis = require('redis');
const redisClient = redis.createClient({ host: config.REDIS_HOST, port: config.REDIS_PORT });
redisClient.on('ready',function() {
    console.log("Redis is ready");
});
redisClient.on('error',function(err) {
    console.log("Error in Redis");
    console.log(err);
});

const rsub = redisClient.duplicate();
const rpub = redisClient.duplicate();
// Связь с драйвером через Redis Pub Sub
rsub.subscribe('from_driver');

const driver_api = function(msg){
    rpub.publish('from_server', msg);
};


// Socket.io_server server init
//const io_server = require('socket.io')(server);
//const io_redis_adapter = require('socket.io-redis');
//io_server.adapter(io_redis_adapter({ host: config.REDIS_HOST, port: config.REDIS_PORT }));


// static files init
//app.use(express.static(__dirname + '/web-ui'));


// JSON parse init
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));



app.get('/api/test', function (req, res){

    //const point = r.point(39,43);

    //r.table('Pilots').indexCreate('loc_coords', {geo: true}).run(function(err){
        //console.log(err);
        //console.log(r.table('Pilots').indexList());

        //r.table('Pilots').indexList().run(function(result){
        //    res.json(result);
        //});

        //
        //Pilot.getNearest(point, 25, 500).then(function(results){
            // results[i].dist, results[i].doc
        //    res.json(results);
        //});

    //});


    res.json({test: 'test'});

});


//
// Состояние компонентов
// Каждую секунду отправляется в браузер
let station_status = {
    r1: 0 // крыша: 0 = ошибка, 1 = закрыта, 2 = открывается, 3 = открыта, 4 = закрывается
    ,d1: 0 // док: 0 = ошибка, 1 = закрыт, 2 = открывается, 3 = открыт, 4 = закрывается, 5 = контакт с дроном, 6 = перегруз по току на моторах)
    ,c1: 0 // зарядка: 0 = выключена, 1 = включена
    ,b1: 0 // ИК маяк: 0 = выключен, 1 = включен
    ,m1: 0 // питание моторов дока: 0 = выключено, 1 = включено
};

const driver_commands = ['roof_open', 'roof_close', 'dock_open', 'dock_close', 'charge_on', 'charge_off'];


const roof = {
    setStatus: function(status){
        station_status.r1 = status;
        socket.emit('station_status', station_status);
    }
};

const dock = {
    setStatus: function(status){
        station_status.d1 = status;
        socket.emit('station_status', station_status);
    }
};

const timeFormat = function(seconds){
	let h = Math.floor( seconds/3600 );
	let m = '0' + Math.floor( (seconds - h*3600)/60 );
	let s = '0' + (seconds - h*3600 - m*60);

    return h + ':' + m.substr(-2) + ':' + s.substr(-2);
};

let charge_data = {
    wh: 0 // вычисленные Ватт*часы
    ,time_start: 0
};
let charge_data_int = null;

const charge = {
    start: function(){
        station_status.c1 = 2; // зарядка включена
        station_status.d1 = 7; // статус Зарядка на доке

        socket.emit('station_status', station_status);

        charge_data.wh = 0;
        charge_data.time_start = (new Date()).getTime();

        charge_data_int = setInterval(function(){
            redisClient.get('charge_v', function(err, vv){
                if( err ) return;

                redisClient.get('charge_i', function(err, vi) {
                    if (err) return;

                    charge_data.wh += vv * vi / 3600; // 3600 = замер раз в секунду

                    socket.emit('charge', {
                        v: vv
                        ,i: vi
                        ,wh: Math.round(charge_data.wh,1)
                        ,t: timeFormat(Math.round( ((new Date()).getTime() - charge_data.time_start)/1000 ))
                    });
                });

            });

        }, 1000); // каждую секунду
    }
    ,stop: function(){
        station_status.c1 = 1;
        station_status.d1 = 5; // статус Контакт на доке

        socket.emit('station_status', station_status);

        clearInterval(charge_data_int);

    }
};


// TODO прочитать из редис
let roof_end_switch_closed = 1;
let roof_end_switch_opened = 0;

let dock_end_switch_closed = 0;
let dock_end_switch_opened = 0;
let dock_end_switch_drone_contact = 0;

// TODO определить состояние компонентов при включении

//
// СОСТОЯНИЕ КРЫШИ
//
// Если закрыто, то ставим просто статус
if( roof_end_switch_closed ){
    station_status.r1 = 1;
}
// Если открыто, то тоже ставим статус
else if( roof_end_switch_opened ){
    station_status.r1 = 3;
}
// Если не определенное положение, то закрываем крышу
else {
    driver_api('roof_close');
}

//
// СОСТОЯНИЕ ДОКА
//
// Если открыт, ставим статус
if( dock_end_switch_opened ){
    station_status.d1 = 3;
}
// Если закрыт, ставим статус
else if( dock_end_switch_closed ){
    station_status.d1 = 1;
}
// Если есть контакт с дроном, ставим статус
else if( dock_end_switch_drone_contact ){
    station_status.d1 = 5;
}
// Если ничего из вышеперечисленного, открываем док
else {
    driver_api('dock_open');
}

// Выключить зарядку
driver_api('charge_off');



//
// Запуск сервера
server.listen(config.API_SERVER_PORT, () => {
    console.log('Listening on port ' + config.API_SERVER_PORT);
});


let send_interval = null;
let send_interval2 = null;

// События соединения socket.io
socket.on('connect', function(){
    console.log('socket.io CONNECTED');

    // Отправить текущее состояние
    socket.emit('station_status', station_status);

    // Отправлять в браузер текущее время каждую секунду
    send_interval = setInterval(function(){
        socket.emit('station_time', (new Date()).getTime());
    }, 1000);

    // Каждые 3 секунды отправляем статус
    send_interval2 = setInterval(function(){
        socket.emit('station_status', station_status);
    }, 3000);

});

// Принимаем команды из браузера и передаем в драйвер
socket.on('command', function(data){

    console.log('command: ' + data);

    if( driver_commands.indexOf(data) !== -1 ) driver_api(data);

    else console.log('unknown command');

});

// Разъединение с сервером socket.io
socket.on('disconnect', function(){
    console.log('socket.io DISCONNECTED');
    clearInterval(send_interval);
    clearInterval(send_interval2);
});


// Получаем сообщения от драйвера
rsub.on('message', function(channel, message) {
    if ('from_driver' !== channel) return;

    console.log('got ' + message);

    // Крыша открывается
    if( message === 'roof_opening' ) roof.setStatus(2);
    // Крыша открыта
    else if( message === 'roof_opened' ) roof.setStatus(3);
    // Крыша закрывается
    else if( message === 'roof_closing' ) roof.setStatus(4);
    // Крыша закрыта
    else if( message === 'roof_closed' ) roof.setStatus(1);
    // Проблемы с крышей
    else if( message === 'roof_error' ) roof.setStatus(0);
    // Док открыт
    else if( message === 'dock_opened' ) dock.setStatus(3);
    // Док закрывается
    else if( message === 'dock_closing' ) dock.setStatus(4);
    // Контакт дока с дроном
    else if( message === 'dock_drone' ) dock.setStatus(5);
    // Док полностью закрыт
    else if( message === 'dock_closed' ) dock.setStatus(1);
    // Док открывается
    else if( message === 'dock_opening' ) dock.setStatus(2);
    // Перегруз моторов в приводе (по току)
    else if( message === 'dock_overloaded' ) dock.setStatus(6);
    // Проблемы с доком
    else if( message === 'dock_error' ) dock.setStatus(0);
    // Зарядка включена
    else if( message === 'charge_start' ) charge.start();
    // Зарядка выключена
    else if( message === 'charge_stop' ) charge.stop();

    // Неизвестная команда
    else console.log('unknown command');

});


// TODO Включить светодиод в режим индикации ОК







//
// остановка скрипта
process.on('SIGINT', function() {
    /*
        процедуры перед остановкой скрипта
     */

    process.exit();

});



/*


sub.on("subscribe", function (channel, count) {
    pub.publish("a nice channel", "I am sending a message.");
    pub.publish("a nice channel", "I am sending a second message.");
    pub.publish("a nice channel", "I am sending my last message.");
});

sub.on("message", function (channel, message) {
    console.log("sub channel " + channel + ": " + message);
    msg_count += 1;
    if (msg_count === 3) {
        sub.unsubscribe();
        sub.quit();
        pub.quit();
    }
});

sub.subscribe("a nice channel");



 */
