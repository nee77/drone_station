'use strict';

// Переменные в Redis
/*
    adc_status
    adc_error
    adc0
    adc1
    adc2
    adc3
    adc4
    adc5
    adc6
    adc7

    charge_v
    charge_i
 */

// Redis API
/*

принимает команды
    dock_drive_on
    dock_drive_off
    dock_drive_open
    dock_drive_close
    dock_drive_stop
    ir_beacon_on
    ir_beacon_off
    charger_on
    charger_off
    horn_on
    horn_off

отправляет события


пишет переменные



 */


// Параметры
const DOCK_MOTOR_I_LIMIT = 450 // TODO Значение АЦП для датчика тока мотора привода дока TODO >= моторы отключаются принудительно
     ,RELAY_STATE_ON = 0 // Состояние выхода для включения реле
     ,RELAY_STATE_OFF = 1 // Состояние выхода для отключения реле
     ,LIMIT_SWITCH_ON = 1 // Состояние замкнутого концевого выключателя
     ,DOCK_MOTOR_PWM_FQ = 100; // Скорость вращения мотора  // 80 // меняем скорость здесь 20 - низкая 160 большая


      // Общие параметры конфигурации
const config = require('./config/config')
      // Распиновка
     ,wires = require('./config/pi_wiring.js');


// Загрузка модулей
const mcpadc = require('mcp-spi-adc')
     ,OnOff = require('onoff').Gpio
     ,Gpio = require('pigpio').Gpio
     ,redis = require('redis');


// Redis client init
const redisClient = redis.createClient({ host: config.REDIS_HOST, port: config.REDIS_PORT });
redisClient.on('ready',function() {
    console.log("Redis is ready");
});
redisClient.on('error',function(err) {
    console.log("Error in Redis");
    console.log(err);
});
const rsub = redisClient.duplicate()
     ,rpub = redisClient.duplicate();

// Связь с драйвером через Redis Pub Sub (канал для прослушки = to_driver)
rsub.subscribe('to_driver');


// Инициализация пинов управления
const relay_dock_motor = new OnOff(wires.GPIO.RELAY_DOCK_MOTOR, 'high')
    ,relay_charger = new OnOff(wires.GPIO.RELAY_CHARGER, 'high')
    ,relay_ir_beacon = new OnOff(wires.GPIO.RELAY_IR_BEACON, 'high')
    ,relay_horn = new OnOff(wires.GPIO.RELAY_HORN, 'high')
    ,status_led = new OnOff(wires.GPIO.STATUS_LED, 'high') // TODO
    ,front_limit_switch = new OnOff(wires.GPIO.DOCK_LS_FRONT, 'in', 'both')
    ,back_limit_switch = new OnOff(wires.GPIO.DOCK_LS_BACK, 'in', 'both')
    ,dock_motor_dir = new OnOff(wires.GPIO.DOCK_MOTOR_1_DIR, 'high')
    ,dock_motor_step = new Gpio(wires.GPIO.DOCK_MOTOR_1_STEP, {mode: Gpio.OUTPUT});


    // Начальные значения АЦП
let adc_data = [0,0,0,0,0,0,0,0]
    // Состояние концевых выключателей
    ,FLS_status = null
    ,BLS_status = null
    // Состояние светодиода статуса
    ,led_blink = 0;

// Установка значения канала АЦП в локальную переменную и в редис
const set_adc = function(channel, data){

    adc_data[channel] = data.rawValue;
    redisClient.set('adc' + channel, adc_data[channel]);

    // Защита мотора от перегрузки по току
    if( wires.ADC.DOCK_I === channel && adc_data[channel] > DOCK_MOTOR_I_LIMIT ) {
        console.log('Motor current limit');
        dock_drive.stop();
    }

};

// Инициализация АЦП
const adc_init = function(){
    let adc_channels = [];

    redisClient.set('adc_status', 'init');

    for( let i = 0; i < 8; i++ ){
        adc_channels[i] = mcpadc.open(i, {speedHz: 20000}, function (err) {
            if (err) {
                redisClient.set('adc_status', 'init_error');
                redisClient.set('adc_error', 'open error on ' + i);
            }
            else {
                redisClient.set('adc_status', 'OK');

                setInterval(function () {
                    adc_channels[i].read(function (err, value) {
                        if (err){
                            redisClient.set('adc_status', 'reading_error');
                            redisClient.set('adc_error', 'read error on ' + i);
                        }
                        else{
                            redisClient.set('adc_status', 'OK');
                            set_adc(i, value);
                        }
                    });
                }, 100);
            }
        });
    }
};
adc_init();

/*
const emulate_adc = function(){

    redisClient.set('adc_status', 'OK');

    setInterval(function(){
        for( let i = 0; i < 8; i++ ){
            set_adc(i, Math.floor(Math.random() * (1000)));
        }
    }, 100);
};
//emulate_adc();
*/


// Инициализация шагового двигателя
dock_motor_step.pwmFrequency(DOCK_MOTOR_PWM_FQ);


//
// определить состояние выключателей на момент запуска скрипта
front_limit_switch.read((err, value) => {
    if( err ) {
        throw err;
    }

    FLS_status = value;

    console.log('FLS init state: ' + FLS_status);
});

back_limit_switch.read((err, value) => {
    if (err) {
        throw err;
    }

    BLS_status = value;

    console.log('BLS init state: ' + BLS_status);
});


//
// Концевые выключатели останавливают привод и ставят свое состояние в глобальные переменные
front_limit_switch.watch((err, value) => {
    if (err) {
        throw err;
    }

    console.log('FLS: ' + value);

    FLS_status = value;

    // Если док закрывался и выключатель замкнулся, то останавливаем привод
    if( LIMIT_SWITCH_ON === value && 'closing' === dock_drive.status ){
        dock_drive.stop();
    }

});

back_limit_switch.watch((err, value) => {
    if (err) {
        throw err;
    }

    console.log('BLS: ' + value);

    BLS_status = value;

    // Если док открывался и выключатель замкнулся, то останавливаем привод
    if( LIMIT_SWITCH_ON === value && 'opening' === dock_drive.status ){
        dock_drive.stop();
    }

});


//
// Включение привода дока, открытие и закрытие
const dock_drive = {

     status: 'stopped'

    ,on: function(){
         console.log('dock motor switch ON');

         this.status = 'stopped';
         this.stop();

         relay_dock_motor.write(RELAY_STATE_ON, function(){});

         return true;
    }

    ,off: function(){
        console.log('dock motor switch OFF');

        this.status = 'off';
        this.stop();
        relay_dock_motor.write(RELAY_STATE_OFF, function(){});

        return true;
    }

    ,open: function(){

        if( 'stopped' !== this.status || BLS_status === LIMIT_SWITCH_ON) return false;

        this.status = 'opening';
        console.log('dock drive opening');

        // привод моторов на открытие
        dock_motor_dir.write(0, function(err){
            if( !err ) dock_motor_step.pwmWrite(20);
        });

        return true;

    }

    ,close: function(){

        if( 'stopped' !== this.status || FLS_status === LIMIT_SWITCH_ON) return false;

        this.status = 'closing';
        console.log('dock drive closing');

        // привод моторов на закрытие
        dock_motor_dir.write(1, function(err){
            if( !err ) dock_motor_step.pwmWrite(20);
        });


        return true;

    }

    ,stop: function(){

        console.log('dock drive stop');
        this.status = 'stopped';

        dock_motor_step.pwmWrite(0);

        return true;

    }

};


//
// Redis API
rsub.on('message', function(channel, message){
    //console.log('got ' + message);
    // Если не нужному каналу, то ничего не делаем
    if( 'to_driver' !== channel ) return;

    // Команды что делать
    if( message === 'dock_drive_on' ) dock_drive.on();
    else if( message === 'dock_drive_off' ) dock_drive.off();
    else if( message === 'dock_drive_open' ) dock_drive.open();
    else if( message === 'dock_drive_close' ) dock_drive.close();
    else if( message === 'dock_drive_stop' ) dock_drive.stop();

    else if( message === 'ir_beacon_on' ){
        console.log('IR beacon ON');
        relay_ir_beacon.write(RELAY_STATE_ON, function(){});
    }
    else if( message === 'ir_beacon_off' ){
        console.log('IR beacon OFF');
        relay_ir_beacon.write(RELAY_STATE_OFF, function(){});
    }

    else if( message === 'charger_on' ){
        console.log('charger ON');
        relay_charger.write(RELAY_STATE_ON, function(){});
    }
    else if( message === 'charger_off' ){
        console.log('charger OFF');
        relay_charger.write(RELAY_STATE_OFF, function(){});
    }

    else if( message === 'horn_on' ){
        console.log('Horn ON');
        relay_horn.write(RELAY_STATE_ON, function(){});
    }
    else if( message === 'horn_off' ){
        console.log('Horn OFF');
        relay_horn.write(RELAY_STATE_OFF, function(){});
    }


    else console.log('unknown command');

});


//
// Мигание светодиодом статуса
setInterval(function(){
    led_blink = (1 === led_blink ? 0 : 1);
    status_led.write(led_blink, function(){});
}, 500);

