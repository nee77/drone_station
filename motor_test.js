const RELAY_STATE_ON = 0;
const RELAY_STATE_OFF = 1;


// Общие параметры конфигурации
const config = require('./config/config');

// Распиновка
const wires = require('./config/pi_wiring.js');


const OnOff = require('onoff').Gpio;
const relay_dock_motor = new OnOff(wires.GPIO.RELAY_DOCK_MOTOR, 'high');

const motor_dir = new OnOff(wires.GPIO.DOCK_MOTOR_1_DIR, 'high');

relay_dock_motor.writeSync(RELAY_STATE_ON);

motor_dir.writeSync(1);




const Gpio = require('pigpio').Gpio;

const led = new Gpio(wires.GPIO.DOCK_MOTOR_1_STEP, {mode: Gpio.OUTPUT});


//led.pwmRange(25);
led.pwmFrequency(80); // меняем скорость здесь 20 - низкая 160 большая
led.pwmWrite(20);


setTimeout(function(){
    led.pwmWrite(0);
}, 3000);


setInterval(function(){}, 1000);

/*


const motor_step = new OnOff(wires.GPIO.DOCK_MOTOR_1_STEP, 'low');




let s = 0;

setTimeout(function(){
    console.log('starting motor');


    motor_step.writeSync(s);

    setInterval(function(){
        s = ( s === 0 ? 1 : 0);
        motor_step.writeSync(s);
        console.log(s);
    }, 10);

}, 1500);



//*/



//
// остановка скрипта
process.on('SIGINT', function() {
    /*
        процедуры перед остановкой скрипта
     */

    relay_dock_motor.writeSync(RELAY_STATE_OFF);
    //motor_step.writeSync(0);

    process.exit();

});
