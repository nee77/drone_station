/*

    Распиновка Raspberry Pi

 */


module.exports = {
    GPIO: {
         DOCK_MOTOR_1_DIR: 23
        ,DOCK_MOTOR_1_STEP: 18
        ,RELAY_DOCK_MOTOR: 5
        ,RELAY_CHARGER: 17
        ,RELAY_IR_BEACON: 22
        ,RELAY_HORN: 27
        ,STATUS_LED: 6
        ,DOCK_LS_FRONT: 26 // Концевой выключатель в центре
        ,DOCK_LS_BACK: 19 // Концевой выключатель скраю
    }

    // Каналы АЦП
    ,ADC: {
         DOCK_ES_FRONT: 0 // концевой выключатель дока ближе к центру
        ,DOCK_ES_BACK: 1 // концевой выключатель дока ближе к краю
        ,CHARGER_V: 5 // напряжение зарядки
        ,CHARGER_I: 6 // ток зарядки
        ,DOCK_I: 7 // ток привода дока
    }
};






