// Общие параметры конфигурации
const config = require('./config/config');

const AUTH = 'f03ddf47ecf049eb97127880fea9d071';

const Blynk = require('blynk-library');


// Redis client init
const redis = require('redis');
const redisClient = redis.createClient({ host: config.REDIS_HOST, port: config.REDIS_PORT });
redisClient.on('ready',function() {
    console.log("Redis is ready");
});
redisClient.on('error',function(err) {
    console.log("Error in Redis");
    console.log(err);
});

const rsub = redisClient.duplicate();
const rpub = redisClient.duplicate();


const sendToDriver = function(msg){
    rpub.publish('to_driver', msg);
};


const blynk = new Blynk.Blynk(AUTH);

// test
const v1 = new blynk.VirtualPin(1);
const v9 = new blynk.VirtualPin(9);


// ADC
const blynk_adc_0 = new blynk.VirtualPin(10);
const blynk_adc_1 = new blynk.VirtualPin(11);
const blynk_adc_2 = new blynk.VirtualPin(12);
const blynk_adc_3 = new blynk.VirtualPin(13);
const blynk_adc_4 = new blynk.VirtualPin(14);
const blynk_adc_5 = new blynk.VirtualPin(15);
const blynk_adc_6 = new blynk.VirtualPin(16);
const blynk_adc_7 = new blynk.VirtualPin(17);

// Dock кнопки
const blynk_btn_doc_open = new blynk.VirtualPin(5);
const blynk_btn_doc_close = new blynk.VirtualPin(6);

// Dock индикаторы
const blynk_led_doc_opened = new blynk.VirtualPin(7);
const blynk_led_doc_closed = new blynk.VirtualPin(8);

// Кнопка питания привода дока
const blynk_btn_dock_onoff = new blynk.VirtualPin(3);

// Кнопка питания ИК маяка
const blynk_btn_ir_onoff = new blynk.VirtualPin(22);

// Кнопка включения ЗУ
const blynk_btn_charger_onoff = new blynk.VirtualPin(23);

// Кнопка включения сирены
const blynk_btn_horn_onoff = new blynk.VirtualPin(24);



v1.on('write', function(param) {
    console.log('V1:', param[0]);
});
v9.on('read', function() {
    v9.write(new Date().getSeconds());
});

const led = new blynk.VirtualPin(2);

let led_status = 0;

setInterval(function(){
    led_status = 0 === led_status ? 255: 0;

    led.write(led_status);
}, 1000);




// прочитать и вывести значения АЦП
setInterval(function(){
    redisClient.get('adc_status', function(err, status) {
        if (err) return;

        if( 'OK' === status ){
            redisClient.get('adc0', function(err, v) {
                if (err) return;

                blynk_adc_0.write(v);
            });

            redisClient.get('adc1', function(err, v) {
                if (err) return;

                blynk_adc_1.write(v);
            });

            redisClient.get('adc2', function(err, v) {
                if (err) return;

                blynk_adc_2.write(v);
            });

            redisClient.get('adc3', function(err, v) {
                if (err) return;

                blynk_adc_3.write(v);
            });

            redisClient.get('adc4', function(err, v) {
                if (err) return;

                blynk_adc_4.write( v );
            });

            redisClient.get('adc5', function(err, v) {
                if (err) return;

                blynk_adc_5.write( Math.round(v/2.77777)/10 );
            });

            redisClient.get('adc6', function(err, v) {
                if (err) return;

                blynk_adc_6.write(v);
            });

            redisClient.get('adc7', function(err, v) {
                if (err) return;

                blynk_adc_7.write(v);
            });


        } else {
            console.log('ADC not available');
        }

    });
}, 500);


// Кнопка Открыть док
blynk_btn_doc_open.on('write', function(param) {

    if( 1 === parseInt(param[0]) )
        sendToDriver('dock_drive_open');
    else
        sendToDriver('dock_drive_stop');

});

// Кнопка Закрыть док
blynk_btn_doc_close.on('write', function(param) {

    if( 1 === parseInt(param[0]) )
        sendToDriver('dock_drive_close');
    else
        sendToDriver('dock_drive_stop');

});

// Реле привода дока
blynk_btn_dock_onoff.on('write', function(param) {

    if( 1 === parseInt(param[0]) )
        sendToDriver('dock_drive_on');
    else
        sendToDriver('dock_drive_off');

});

// Реле ИК маяка
blynk_btn_ir_onoff.on('write', function(param) {

    if( 1 === parseInt(param[0]) )
        sendToDriver('ir_beacon_on');
    else
        sendToDriver('ir_beacon_off');

});

// Реле ЗУ
blynk_btn_charger_onoff.on('write', function(param) {

    if( 1 === parseInt(param[0]) )
        sendToDriver('charger_on');
    else
        sendToDriver('charger_off');

});

// Реле сирены
blynk_btn_horn_onoff.on('write', function(param) {

    if( 1 === parseInt(param[0]) )
        sendToDriver('horn_on');
    else
        sendToDriver('horn_off');

});