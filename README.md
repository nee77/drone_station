# drone_station

Скрипт для зарядной станции дрона


bme280-sensor NodeJS
https://github.com/skylarstein/bme280-sensor



# Установка



https://howchoo.com/g/ndy1zte2yjn/how-to-set-up-wifi-on-your-raspberry-pi-without-ethernet




## USB флешка ##

посмотреть список флешек

    ls -l /dev/disk/by-uuid/

сделать папку и примонтировать флешку

    sudo mkdir /media/usb
    sudo chown -R pi:pi /media/usb
    sudo mount /dev/sda1 /media/usb -o uid=pi,gid=pi

в файле

    sudo nano /etc/fstab

прописать (заменить UUID флешки на нужный) для автомонитрования при старте

    UUID=1234-ABCD  /media/usb vfat auto,nofail,noatime,users,rw,uid=pi,gid=pi 0 0

Сохранить *Ctrl-X*, y
и перегрузить компьютер

    sudo reboot


## Redis

    sudo apt-get install redis-server

Конфигурация

    sudo nano /etc/redis/redis.conf

Параметры каталога для БД

    dir /PATH/TO/DIR

Перезапуск процесса

    sudo systemctl status redis
    sudo systemctl restart redis
    sudo systemctl start redis



    logfile /var/log/redis/redis-server.log



## Node JS

    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt-get install -y nodejs build-essential pigpio
    
    # npm install node-wiring-pi



## Менеджер процессов PM2

    sudo npm install pm2 -g
    pm2 install pm2-logrotate
    pm2 startup



## MAVProxy

    sudo apt-get install python-opencv python-wxgtk2.8 python-pip python-dev python-lxml python3-lxml
    sudo pip install MAVProxy
 


## drone_station

    git clone https://nee77@bitbucket.org/nee77/drone_station.git
    cd drone_station
    npm install

Сделать скрипты mavproxy.sh и video.sh исполняемыми

    chmod +x mavproxy.sh video.sh

Конфигурация

    nano config.js


## Gstreamer

    sudo apt-get install gstreamer1.0-tools gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly


## Ключи для трансляции видео и телеметрии на сервер

В панели управления GCS.uno необходимо зарегистрировать дрон и прописать ключи в глобальные переменные

телеметрия 

    DRONE_MAVLINK_KEY=abcd1234; echo "export DRONE_MAVLINK_KEY=$DRONE_MAVLINK_KEY" >>~/.bash_profile && source ~/.bash_profile
    
видео    
    
    DRONE_VIDEO_KEY=abcd1234; echo "export DRONE_VIDEO_KEY=$DRONE_VIDEO_KEY" >>~/.bash_profile && source ~/.bash_profile



## Запуск

MAVProxy
    
    pm2 start scripts/mavproxy.sh --name mavproxy
    
Скрипт для трансляции телеметрии дрона на сервер    
    
    pm2 start mavlink.js
    
    
    
    pm2 start driver.js
    pm2 start station.js
    
    pm2 save
    pm2 startup


Найти Raspberry в локальной сети

    sudo nmap -sP 192.168.1.0/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}'
    
    arp -a | grep :
    



https://computers.tutsplus.com/articles/how-to-clone-raspberry-pi-sd-cards-using-the-command-line-in-os-x--mac-59911

    sudo dd if=/dev/disk3 of=/dev/disk2/raspberrypi.dmg



# Docs

wait-until
https://github.com/nylen/wait-until


